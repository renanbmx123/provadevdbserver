import {Restaurant} from '../entities/restaurant';
import {RestaurantModel} from '../models/restaurantModel';

export async function newRestaurant(restaurant: Restaurant): Promise<Restaurant> {
    return RestaurantModel.create(restaurant);
}

export async function getRestaurant(): Promise<Restaurant []> {
    return await RestaurantModel.find().sort({name: 'desc'}).populate('restaurant', RestaurantModel).exec();
}
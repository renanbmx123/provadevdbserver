
import app from './app';
import {connect} from 'mongoose';

(async () => {
    try {
        // const mongoDbServer = `mongodb://${process.env.ATLAS_USER}:${process.env.ATLAS_PASSWORD}@${process.env.ATLAS_CLUSTER}/${process.env.ATLAS_DATABASE}?retryWrites=true&w=majority`;
        const mongoDbServer = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`; 
        //const mongoDbServer = 'mongodb://localhost:27017/prova_db';
        try {
            await connect(mongoDbServer, { useNewUrlParser: true, useUnifiedTopology: true });
        } catch (erro) {
            console.log(`Erro: ${erro.message}`);    
        }
    app.listen(app.get('port'), () => {
        console.log(`Express running on port ${app.get('port')}`);
    });

    } catch (erro) {
        console.log(`Error: ${erro.message}`);
    }
})();

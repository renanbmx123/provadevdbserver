import {Router} from 'express';
import * as RestaurantController from '../controllers/RestaurantController';

export const router = Router();
export const path   = '/restaurant';

router.put ('', RestaurantController.insertRestaurant );
router.get ('', RestaurantController.getRestaurant);
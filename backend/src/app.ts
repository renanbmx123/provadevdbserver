import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import * as userRoutes from "./routes/userRoutes";
import * as restaurantRoutes from "./routes/restrauntRoute";

const app = express();

app.set("port", 3000);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(userRoutes.path, userRoutes.router);
app.use(restaurantRoutes.path, restaurantRoutes.router);
export default app;

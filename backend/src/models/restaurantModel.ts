import { Schema, model, Document } from 'mongoose';
import {Restaurant} from '../entities/restaurant';

interface RestaurantDocument extends Document, Restaurant{}
export const RestaurantModel = model<RestaurantDocument>('restaurant', new Schema({
    name:   { type: String, required: true},
    location:   { type: String, required: true},
}));
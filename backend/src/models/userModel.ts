import { Schema, model, Document } from 'mongoose';
import {User} from '../entities/user';

interface UserDocument extends Document, User{}
export const userModel = model<UserDocument>('user', new Schema({
    name:   { type: String, required: true},
    user:   { type: String, required: true},
    avatar: { type: String, required: false}
}));
import { Request, Response, NextFunction } from "express";
import * as RestaurantRepo from '../persistence/restaurantRepo';
import {Restaurant} from '../entities/restaurant';

export async function insertRestaurant(req: Request, res: Response, next: NextFunction): Promise<void>{
    try {
        const newRestaurant = req.body as Restaurant;
        const CreateRestaurant = await RestaurantRepo.newRestaurant(newRestaurant);
        res.status(201).json(CreateRestaurant);  
    } catch (erro) {
        next(erro);
    }
}

export async function getRestaurant(req: Request, res: Response, next: NextFunction) {
    try {
        const restaurant = await RestaurantRepo.getRestaurant();
        if (restaurant.length ===0){
            res.status(404).json(restaurant).send('Not able to find any restaurant');
        }
        res.status(200).json(restaurant);
    } catch (error) {
        next(error);
    }

}
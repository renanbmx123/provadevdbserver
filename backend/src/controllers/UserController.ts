import {User} from '../entities/user';
import {userModel} from '../models/userModel';

export async function newUser(user: User): Promise<User> {
    return userModel.create(user);
}

export async function searchUser(userName: string): Promise<User | null> {
    return await userModel.findOne().where("user").equals(userName).exec();
}
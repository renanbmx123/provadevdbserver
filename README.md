# ProvaDevDBServer

# Requirements  
NODEJS
ANGULAR 8
NPM

# Usage

 First we need to install all the necessary packages in order to run our project.
 On the ´backend´ and ´frontend´ we need to run the following command to install all depencies:
 ´npm install´ after that we can run ´npm run dev´

# Packages
### Backend
mongoose
express
axios

